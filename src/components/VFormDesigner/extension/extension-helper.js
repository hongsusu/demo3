import {
  addContainerWidgetSchema,
  addBasicFieldSchema,
  addAdvancedFieldSchema,
  addCustomWidgetSchema
} from '@VFormDesigner/components/form-designer/widget-panel/widgetsConfig'
import {
  registerCommonProperty,
  registerAdvancedProperty,
  registerEventProperty
} from '@VFormDesigner/components/form-designer/setting-panel/propertyRegister'


export default {
  addContainerWidgetSchema,
  addBasicFieldSchema,
  addAdvancedFieldSchema,
  addCustomWidgetSchema,

  registerCommonProperty,
  registerAdvancedProperty,
  registerEventProperty,
}
