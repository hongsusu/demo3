import store from "@/store/index";
import router from './router'
import { createApp } from 'vue'
import App from './App.vue'
const app = createApp(App)

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import * as icons from '@element-plus/icons'
app.use(ElementPlus, { size: 'default', locale: zhCn })
Object.keys(icons).forEach(key => {
    app.component(key, icons[key])
})

import MyPD from './package/index.js';
app.use(MyPD);
import './package/theme/index.scss';

//新增form-create
import axios from 'axios'
if (typeof window !== 'undefined') {
    window.axios = axios
}
import Draggable from '@VFormDesigner/lib/vuedraggable/vuedraggable.umd.js'
import {registerIcon} from '@VFormDesigner/utils/el-icons'
import 'virtual:svg-icons-register'

import ContainerWidgets from '@VFormDesigner/components/form-designer/form-widget/container-widget/index'
import ContainerItems from '@VFormDesigner/components/form-render/container-item/index'

import { addDirective } from '@VFormDesigner/utils/directive'
import { installI18n } from '@VFormDesigner/utils/i18n'
import { loadExtension } from '@VFormDesigner/extension/extension-loader'

registerIcon(app)
app.component('draggable', Draggable)
addDirective(app)
installI18n(app)

app.use(ContainerWidgets)
app.use(ContainerItems)
loadExtension(app)

//新增form-creat end

app.use(store)
app.use(router)
app.mount('#app')
