import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        redirect: '/home',

    },

    {
        path: "/home",
        name: "index",
        meta: {
            title: "home",
        },
        component:  () => import("@/pages/home/index.vue"),
    },
    {
        path: "/demo2",
        name: "demo2",
        meta: {
            title: "demo2",
        },
        component:  () => import("@/pages/home/demo2.vue"),
    },

    {
        path: "/login",
        name: "login",
        meta: {
            title: "login",
        },
        component: () => import("@/pages/login/index.vue"),
    },
    {
        path: "/renderDetails",
        name: "renderDetails",
        meta: {
            title: "renderDetails",
        },
        component: () => import("@/components/VFormDesigner/components/custom/render-details.vue"),
    },
    {
        path: "/useRender",
        name: "useRender",
        meta: {
            title: "useRender",
        },
        component: () => import("@/pages/login/useRender.vue"),
    },
    {
        path: "/iframe",
        name: "iframe",
        meta: {
            title: "iframe",
        },
        component: () => import("@/pages/iframe.vue"),
    },
    {
        path: "/iframevue",
        name: "iframevue",
        meta: {
            title: "iframevue",
        },
        component: () => import("@/pages/iframevue.vue"),
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});
export default router;
