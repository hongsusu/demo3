// vite.config.ts
import { defineConfig } from "file:///D:/work/demo/demo3/vite-project/node_modules/vite/dist/node/index.js";
import vue from "file:///D:/work/demo/demo3/vite-project/node_modules/@vitejs/plugin-vue/dist/index.mjs";
import { resolve } from "path";
import vueJsx from "file:///D:/work/demo/demo3/vite-project/node_modules/@vitejs/plugin-vue-jsx/index.js";
import { createSvgIconsPlugin } from "file:///D:/work/demo/demo3/vite-project/node_modules/vite-plugin-svg-icons/dist/index.mjs";
var __vite_injected_original_dirname = "D:\\work\\demo\\demo3\\vite-project";
var vite_config_default = defineConfig({
  plugins: [
    vue(),
    vueJsx({}),
    createSvgIconsPlugin({
      // Specify the icon folder to be cached
      iconDirs: [resolve(process.cwd(), "src/components/VFormDesigner/icons/svg")],
      // Specify symbolId format
      symbolId: "icon-[dir]-[name]"
    })
  ],
  resolve: {
    alias: {
      "~": resolve(__vite_injected_original_dirname, "./"),
      "@": resolve(__vite_injected_original_dirname, "src"),
      "@VFormDesigner": resolve(__vite_injected_original_dirname, "src/components/VFormDesigner"),
      "src": resolve(__vite_injected_original_dirname, "src")
    },
    extensions: [".js", ".ts", ".jsx", ".tsx", ".json", ".vue", ".mjs"]
  },
  optimizeDeps: {
    include: ["@VFormDesigner/lib/vuedraggable/vuedraggable.umd.js", "quill"]
  },
  css: {
    preprocessorOptions: {
      scss: {
        /* 自动引入全局scss文件 */
        additionalData: '@import "./src/components/VFormDesigner/styles/global.scss";'
      }
    }
  }
});
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcudHMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCJEOlxcXFx3b3JrXFxcXGRlbW9cXFxcZGVtbzNcXFxcdml0ZS1wcm9qZWN0XCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ZpbGVuYW1lID0gXCJEOlxcXFx3b3JrXFxcXGRlbW9cXFxcZGVtbzNcXFxcdml0ZS1wcm9qZWN0XFxcXHZpdGUuY29uZmlnLnRzXCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ltcG9ydF9tZXRhX3VybCA9IFwiZmlsZTovLy9EOi93b3JrL2RlbW8vZGVtbzMvdml0ZS1wcm9qZWN0L3ZpdGUuY29uZmlnLnRzXCI7aW1wb3J0IHsgZGVmaW5lQ29uZmlnIH0gZnJvbSAndml0ZSdcbmltcG9ydCB2dWUgZnJvbSAnQHZpdGVqcy9wbHVnaW4tdnVlJ1xuaW1wb3J0IHsgcmVzb2x2ZSB9IGZyb20gJ3BhdGgnXG5pbXBvcnQgdnVlSnN4IGZyb20gJ0B2aXRlanMvcGx1Z2luLXZ1ZS1qc3gnXG5pbXBvcnQge2NyZWF0ZVN2Z0ljb25zUGx1Z2lufSBmcm9tICd2aXRlLXBsdWdpbi1zdmctaWNvbnMnO1xuXG4vLyBodHRwczovL3ZpdGVqcy5kZXYvY29uZmlnL1xuZXhwb3J0IGRlZmF1bHQgZGVmaW5lQ29uZmlnKHtcbiAgICBwbHVnaW5zOiBbXG4gICAgICAgIHZ1ZSgpLFxuICAgICAgICB2dWVKc3goe30pLFxuICAgICAgICBjcmVhdGVTdmdJY29uc1BsdWdpbih7XG4gICAgICAgIC8vIFNwZWNpZnkgdGhlIGljb24gZm9sZGVyIHRvIGJlIGNhY2hlZFxuICAgICAgICBpY29uRGlyczogW3Jlc29sdmUocHJvY2Vzcy5jd2QoKSwgJ3NyYy9jb21wb25lbnRzL1ZGb3JtRGVzaWduZXIvaWNvbnMvc3ZnJyldLFxuICAgICAgICAvLyBTcGVjaWZ5IHN5bWJvbElkIGZvcm1hdFxuICAgICAgICBzeW1ib2xJZDogJ2ljb24tW2Rpcl0tW25hbWVdJyxcbiAgICB9KVxuICAgIF0sXG4gICAgcmVzb2x2ZToge1xuICAgICAgICBhbGlhczoge1xuICAgICAgICAgICAgJ34nOiByZXNvbHZlKF9fZGlybmFtZSwgJy4vJyksXG4gICAgICAgICAgICAnQCc6IHJlc29sdmUoX19kaXJuYW1lLCAnc3JjJyksXG4gICAgICAgICAgICAnQFZGb3JtRGVzaWduZXInOiByZXNvbHZlKF9fZGlybmFtZSwgJ3NyYy9jb21wb25lbnRzL1ZGb3JtRGVzaWduZXInKSxcbiAgICAgICAgICAgICdzcmMnOiByZXNvbHZlKF9fZGlybmFtZSwgJ3NyYycpXG4gICAgICAgIH0sXG4gICAgICAgIGV4dGVuc2lvbnM6IFsnLmpzJywgJy50cycsICcuanN4JywgJy50c3gnLCAnLmpzb24nLCAnLnZ1ZScsICcubWpzJ11cbiAgICB9LFxuICAgIG9wdGltaXplRGVwczoge1xuICAgICAgICBpbmNsdWRlOiBbJ0BWRm9ybURlc2lnbmVyL2xpYi92dWVkcmFnZ2FibGUvdnVlZHJhZ2dhYmxlLnVtZC5qcycsICdxdWlsbCddXG4gICAgfSxcbiAgICBjc3M6IHtcbiAgICAgICAgcHJlcHJvY2Vzc29yT3B0aW9uczoge1xuICAgICAgICAgICAgc2Nzczoge1xuICAgICAgICAgICAgICAgIC8qIFx1ODFFQVx1NTJBOFx1NUYxNVx1NTE2NVx1NTE2OFx1NUM0MHNjc3NcdTY1ODdcdTRFRjYgKi9cbiAgICAgICAgICAgICAgICBhZGRpdGlvbmFsRGF0YTogJ0BpbXBvcnQgXCIuL3NyYy9jb21wb25lbnRzL1ZGb3JtRGVzaWduZXIvc3R5bGVzL2dsb2JhbC5zY3NzXCI7J1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufSlcbiJdLAogICJtYXBwaW5ncyI6ICI7QUFBeVIsU0FBUyxvQkFBb0I7QUFDdFQsT0FBTyxTQUFTO0FBQ2hCLFNBQVMsZUFBZTtBQUN4QixPQUFPLFlBQVk7QUFDbkIsU0FBUSw0QkFBMkI7QUFKbkMsSUFBTSxtQ0FBbUM7QUFPekMsSUFBTyxzQkFBUSxhQUFhO0FBQUEsRUFDeEIsU0FBUztBQUFBLElBQ0wsSUFBSTtBQUFBLElBQ0osT0FBTyxDQUFDLENBQUM7QUFBQSxJQUNULHFCQUFxQjtBQUFBO0FBQUEsTUFFckIsVUFBVSxDQUFDLFFBQVEsUUFBUSxJQUFJLEdBQUcsd0NBQXdDLENBQUM7QUFBQTtBQUFBLE1BRTNFLFVBQVU7QUFBQSxJQUNkLENBQUM7QUFBQSxFQUNEO0FBQUEsRUFDQSxTQUFTO0FBQUEsSUFDTCxPQUFPO0FBQUEsTUFDSCxLQUFLLFFBQVEsa0NBQVcsSUFBSTtBQUFBLE1BQzVCLEtBQUssUUFBUSxrQ0FBVyxLQUFLO0FBQUEsTUFDN0Isa0JBQWtCLFFBQVEsa0NBQVcsOEJBQThCO0FBQUEsTUFDbkUsT0FBTyxRQUFRLGtDQUFXLEtBQUs7QUFBQSxJQUNuQztBQUFBLElBQ0EsWUFBWSxDQUFDLE9BQU8sT0FBTyxRQUFRLFFBQVEsU0FBUyxRQUFRLE1BQU07QUFBQSxFQUN0RTtBQUFBLEVBQ0EsY0FBYztBQUFBLElBQ1YsU0FBUyxDQUFDLHVEQUF1RCxPQUFPO0FBQUEsRUFDNUU7QUFBQSxFQUNBLEtBQUs7QUFBQSxJQUNELHFCQUFxQjtBQUFBLE1BQ2pCLE1BQU07QUFBQTtBQUFBLFFBRUYsZ0JBQWdCO0FBQUEsTUFDcEI7QUFBQSxJQUNKO0FBQUEsRUFDSjtBQUNKLENBQUM7IiwKICAibmFtZXMiOiBbXQp9Cg==
