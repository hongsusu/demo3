import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import vueJsx from '@vitejs/plugin-vue-jsx'
import {createSvgIconsPlugin} from 'vite-plugin-svg-icons';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        vueJsx({}),
        createSvgIconsPlugin({
        // Specify the icon folder to be cached
        iconDirs: [resolve(process.cwd(), 'src/components/VFormDesigner/icons/svg')],
        // Specify symbolId format
        symbolId: 'icon-[dir]-[name]',
    })
    ],
    resolve: {
        alias: {
            '~': resolve(__dirname, './'),
            '@': resolve(__dirname, 'src'),
            '@VFormDesigner': resolve(__dirname, 'src/components/VFormDesigner'),
            '@FlowableBpmn': resolve(__dirname, 'src/components/FlowableBpmn'),
            'src': resolve(__dirname, 'src')
        },
        extensions: ['.js', '.ts', '.jsx', '.tsx', '.json', '.vue', '.mjs']
    },
    server: {
        hmr: true, // 配置自动刷新
        port: 3000
    },
    optimizeDeps: {
        include: ['@VFormDesigner/lib/vuedraggable/vuedraggable.umd.js', 'quill']
    },
    css: {
        preprocessorOptions: {
            scss: {
                /* 自动引入全局scss文件 */
                additionalData: '@import "./src/components/VFormDesigner/styles/global.scss";'
            }
        }
    }
})
